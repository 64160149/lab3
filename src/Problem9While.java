import java.util.Scanner;

public class Problem9While {
    public static void main(String[] args) {
        int n;
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input number: ");
        n = sc.nextInt();
        int i = 1;
        while (i < n+1) {
            int j = 1;
            while (j < n+1) {
                System.out.print(j);
                j++;
            }    
            System.out.println();
            i++;    
        }
    }
}